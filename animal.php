<?php

class Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";

    public function __construct($string)
    {
        $this->name = $string;
    }
}

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


?>