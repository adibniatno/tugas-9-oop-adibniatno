<?php
 require_once("animal.php");
 require_once("Ape.php");
 require_once("Frog.php");

$sheep = new Animal("shaun");
echo "Nama Hewan: ". $sheep->name."<br>"; // "shaun"
echo "Jumlah Kaki: ".$sheep->legs."<br>"; // 2
echo "Berdarah dingin: ". $sheep->cold_blooded."<br>"; // false

echo "<br>";

// index.php
$kodok = new Frog("buduk");
echo "Nama Hewan: ".$kodok->name."<br>";
echo "Jumlah Kaki: ".$kodok->legs."<br>";
echo "Berdarah dingin: ". $kodok->cold_blooded."<br>";
echo $kodok->jump()."<br>"; // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: ". $sungokong->name."<br>";
echo "Jumlah Kaki: ".$sungokong->legs."<br>";
echo "Berdarah dingin: ". $sungokong->cold_blooded."<br>";
echo $sungokong->yell();  // "Auooo"

?>
